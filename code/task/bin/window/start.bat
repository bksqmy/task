@echo off & setlocal enabledelayedexpansion

title task

% 启动 %
echo Starting ...

set project.dir=${user.dir}/../../
java -Xms256m -Xmx256m -XX:MaxPermSize=64M -Dproject.dir=%project.dir% -jar ..\..\task-2.0.2.war

:end
pause